FROM node:lts as builder

ADD . .
RUN yarn nx build blog --with-deps --skip-nx-cache --prefixPaths
