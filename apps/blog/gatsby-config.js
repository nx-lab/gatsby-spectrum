const options = {
  basePath: '/',
  contentPath: `content/posts`,
  assetPath: `content/assets`,
  excerptLength: 140,
  imageMaxWidth: 1380,
  filter: {},
  limit: 1000,
};
module.exports = {
  siteMetadata: {
    title: `We build micro frontend!`,
    author: `Watheia Labs, LLC`,
    description: `Watheia Labs, LLC is a modern engineering and digital design agency offering consulting services in Southeast Washington State`,
    siteUrl: `https://blog.watheia.io`,
    social: [
      {
        name: `Twitter`,
        url: `https://twitter.com/watheia`,
      },
      {
        name: `GitHub`,
        url: `https://github.com/watheia`,
      },
    ],
  },
  plugins: [
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: options.imageMaxWidth,
              linkImagesToOriginal: false,
            },
          },
          { resolve: `gatsby-remark-copy-linked-files` },
          { resolve: `gatsby-remark-smartypants` },
        ],
        remarkPlugins: [require(`remark-slug`)],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `content/posts`,
        name: `content/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: options.assetPath || `content/assets`,
        name: options.assetPath || `content/assets`,
      },
    },
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        svgo: false,
        ref: true,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: require.resolve(`@nrwl/gatsby/plugins/nx-gatsby-ext-plugin`),
      options: {
        path: __dirname,
      },
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `blog`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#111111`,
        theme_color: `downRiverBlue`,
        display: `minimal-ui`,
        icon: `src/images/logo.svg`,
      },
    },
  ],
};
