import { useStaticQuery, graphql } from 'gatsby';

const useBlogThemeConfig = () => {
  const data = useStaticQuery(graphql`
    query {
      blogThemeConfig(
        id: { eq: "@watheia/nx-lab.gatsby.spectrum.blog-config" }
      ) {
        webfontURL
      }
    }
  `);

  return data.blogThemeConfig;
};

export default useBlogThemeConfig;
