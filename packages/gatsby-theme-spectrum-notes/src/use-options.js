import { graphql, useStaticQuery } from 'gatsby';

const useOptions = () => {
  const data = useStaticQuery(graphql`
    {
      notesConfig(id: { eq: "@watheia/nx-lab.gatsby.spectrum.notes-config" }) {
        basePath
        homeText
        breadcrumbSeparator
      }
    }
  `);

  return data.notesConfig;
};

export default useOptions;
