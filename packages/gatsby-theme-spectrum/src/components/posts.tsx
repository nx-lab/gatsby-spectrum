import * as React from 'react';
import PostList from './post-list';

const Posts = ({ data }) => {
  const posts = data.allBlogPost.nodes;
  return (
    <Content>
      <PostList posts={posts} />
    </Content>
  );
};

export default Posts;
